def change(n):
    coins = [100, 50, 20, 10, 5, 2, 1]  # 硬币面值，单位为分
    counts = [0] * len(coins)  # 记录每种硬币的数量
    for i, coin in enumerate(coins):
        counts[i] = n // coin  # 计算当前硬币种类的数量
        n %= coin  # 更新需要找的零钱数目
    return counts
print(change(97))