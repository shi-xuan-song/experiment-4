def combine(n, k):
    def backtrack(first = 1, curr = []):
        if len(curr) == k:
            output.append(curr[:])
        for i in range(first, n + 1):
            curr.append(i)
            backtrack(i + 1, curr)
            curr.pop()
    output = []
    backtrack()
    return output
combinations = combine(5, 3)
for combination in combinations:
    print(combination)