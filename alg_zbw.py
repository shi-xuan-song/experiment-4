from typing import List
from scipy import optimize
optimize.linprog()


# leetcode 123.买卖股票的最佳时机3 动态规划 困难
def maxProfit(prices: List[int]) -> int:
    if not prices:
        return 0
    length = len(prices)
    # 结束时的最高利润=[天数][是否持有股票][卖出次数]
    status_record = [[[0, 0, 0], [0, 0, 0]] for _ in range(0, length)]  # 先初始化存储列表
    # 第一天休息
    status_record[0][0][0] = 0
    # 第一天买入
    status_record[0][1][0] = -prices[0]
    # 第一天不可能已经有卖出
    status_record[0][0][1] = float('-inf')
    status_record[0][0][2] = float('-inf')
    status_record[0][1][1] = float('-inf')
    status_record[0][1][2] = float('-inf')
    for i in range(1, length):  # 第一天的数据手动写入了，直接从第二天开始算
        # 未持股，未卖出过，说明从未进行过买卖
        status_record[i][0][0] = 0
        # 未持股，卖出过1次，可能是今天卖的，可能是之前卖的
        status_record[i][0][1] = max(status_record[i - 1][1][0] + prices[i], status_record[i - 1][0][1])
        # 未持股，卖出过2次，可能是今天卖的，可能是之前卖的
        status_record[i][0][2] = max(status_record[i - 1][1][1] + prices[i], status_record[i - 1][0][2])
        # 持股，未卖出过，可能是今天买的，可能是之前买的
        status_record[i][1][0] = max(status_record[i - 1][0][0] - prices[i], status_record[i - 1][1][0])
        # 持股，卖出过1次，可能是今天买的，可能是之前买的
        status_record[i][1][1] = max(status_record[i - 1][0][1] - prices[i], status_record[i - 1][1][1])
        # 持股，卖出过2次，还买？傍富婆了？
        status_record[i][1][2] = float('-inf')
    return max(status_record[length - 1][0][1], status_record[length - 1][0][2], 0)


# leetcode 2136.全部开花的最早一天 贪心算法 困难
def earliestFullBloom(plant_time: List[int], grow_time: List[int]) -> int:
    result = day = 0
    for plant, grow in sorted(zip(plant_time, grow_time), key=lambda x: -x[1]):
        day += plant
        result = max(result, day + grow)
    return result


# leetcode 1219.黄金矿工 回溯算法 中等
def getMaximumGold(grid: List[List[int]]) -> int:
    m, n = len(grid), len(grid[0])
    result = 0

    # 以x, y为起点回溯查找最大值
    def fun(x: int, y: int, gold: int):
        gold += grid[x][y]
        nonlocal result
        result = max(result, gold)

        # 每走一个点就先把这个点置0，把点里原来的值保存，之后回溯时候再还回去
        rec = grid[x][y]
        grid[x][y] = 0

        # 上下左右四个方向递归一遍
        for nx, ny in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= nx < m and 0 <= ny < n and grid[nx][ny] > 0:
                fun(nx, ny, gold)

        # 把值还回去
        grid[x][y] = rec

    # 以每个点为起点，都调用一遍函数
    for i in range(m):
        for j in range(n):
            if grid[i][j] != 0:
                fun(i, j, 0)

    return result


print(maxProfit([3, 3, 5, 0, 0, 3, 1, 4]))  # leetcode 123.买卖股票的最佳时机3 动态规划 困难 测试语句
print(earliestFullBloom([1, 4, 3], [2, 3, 1]))  # leetcode 2136.全部开花的最早一天 贪心算法 困难 测试语句
print(getMaximumGold([[0, 6, 0], [5, 8, 7], [0, 9, 0]]))  # leetcode 1219.黄金矿工 回溯算法 中等 测试语句
