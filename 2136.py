def getMaximumGold(grid: List[List[int]]) -> int:
    m, n = len(grid), len(grid[0])
    result = 0
    # 以x, y为起点回溯查找最大值
    def fun(x: int, y: int, gold: int):
        gold += grid[x][y]
        nonlocal result
        result = max(result, gold)
        # 每走一个点就先把这个点置0，把点里原来的值保存，之后回溯时候再还回去
        rec = grid[x][y]
        grid[x][y] = 0
        # 上下左右四个方向递归一遍
        for nx, ny in ((x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)):
            if 0 <= nx < m and 0 <= ny < n and grid[nx][ny] > 0:
                fun(nx, ny, gold)
        # 把值还回去
        grid[x][y] = rec
    # 以每个点为起点，都调用一遍函数
    for i in range(m):
        for j in range(n):
            if grid[i][j] != 0:
                fun(i, j, 0)
    return result