def earliestFullBloom(plant_time: List[int], grow_time: List[int]) -> int:
    result = day = 0
    for plant, grow in sorted(zip(plant_time, grow_time), key=lambda x: -x[1]):
        day += plant
        result = max(result, day + grow)
    return result