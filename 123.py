def maxProfit(prices: List[int]) -> int:
    if not prices:
        return 0
    length = len(prices)
    # 结束时的最高利润=[天数][是否持有股票][卖出次数]
    status_record = [[[0, 0, 0], [0, 0, 0]] for _ in range(0, length)]  # 先初始化存储列表
    # 第一天休息
    status_record[0][0][0] = 0
    # 第一天买入
    status_record[0][1][0] = -prices[0]
    # 第一天不可能已经有卖出
    status_record[0][0][1] = float('-inf')
    status_record[0][0][2] = float('-inf')
    status_record[0][1][1] = float('-inf')
    status_record[0][1][2] = float('-inf')
    for i in range(1, length):  # 第一天的数据手动写入了，直接从第二天开始算
        # 未持股，未卖出过，说明从未进行过买卖
        status_record[i][0][0] = 0
        # 未持股，卖出过1次，可能是今天卖的，可能是之前卖的
        status_record[i][0][1] = max(status_record[i - 1][1][0] + prices[i], status_record[i - 1][0][1])
        # 未持股，卖出过2次，可能是今天卖的，可能是之前卖的
        status_record[i][0][2] = max(status_record[i - 1][1][1] + prices[i], status_record[i - 1][0][2])
        # 持股，未卖出过，可能是今天买的，可能是之前买的
        status_record[i][1][0] = max(status_record[i - 1][0][0] - prices[i], status_record[i - 1][1][0])
        # 持股，卖出过1次，可能是今天买的，可能是之前买的
        status_record[i][1][1] = max(status_record[i - 1][0][1] - prices[i], status_record[i - 1][1][1])
        # 持股，卖出过2次，还买？傍富婆了？
        status_record[i][1][2] = float('-inf')
    return max(status_record[length - 1][0][1], status_record[length - 1][0][2],