def min_refuel_stops(n, k, stations):
    num_refuel = 0  # 记录加油的次数
    prev = 0  # 记录上一次加油的位置
    pq = []  # 创建一个堆来存储可以加油的加油站
    for i, (location, capacity) in enumerate(stations + [(n, 0)]):
        fuel = location - prev  # 当前位置到上一个加油站的距离
        while pq and fuel < 0:
            # 如果当前位置到上一个加油站的距离大于当前油箱的容量，就加油
            fuel += -heapq.heappop(pq)
            num_refuel += 1
        if fuel < 0:  # 如果还是不够到达下一个加油站，就无法到达终点
            return -1
        heapq.heappush(pq, -capacity)  # 将当前加油站的油量加入堆中
        prev = location  # 更新上一次加油的位置
    return num_refuel
n = 1000
k = 5
stations = [(150, 400), (200, 300), (350, 200), (400, 250), (600, 600)]
print(min_refuel_stops(n, k, stations))